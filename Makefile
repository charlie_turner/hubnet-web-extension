#
# Makefile for hubnet-web.jar NetLogo Extension
#
# updated to use:
#
#	Scala 2.9.3
#	sbt 0.13.5
#	unfiltered 0.6.8
#
# Charlie Turner, UC Davis
# September 29, 2014
#
# Note: had to add one line to build.sbt in order to get libraries into the "managed" library folder lib_managed
# retrieveManaged := true
# They are normally kept in repositories in ~/.ivy/ and other similar places

# sbt will drop files in target/SCALA_VERSION/
SCALA_VERSION=scala-2.9.2

# you will probably need to adjust the NetLogo path (to find Netlogo.jar)
ifeq ($(origin NETLOGO), undefined)
  NETLOGO=../../NetLogo_5.0.5
endif

ifeq ($(origin SCALA_HOME), undefined)
  SCALA_HOME=../..
endif

TARGET 		= hubnet-web.jar
ZIPTARGET	= hubnet-web

SRCS=$(wildcard src/main/scala/*.scala)

ZIP_SOURCES=hubnet-web

NETLOGO_JAR=NetLogo.jar

hubnet-web.jar: $(SRCS) manifests/web.txt Makefile
	# make sure we have the current NetLogo.jar in the "unmanaged" lib directory
	mkdir -p lib
	mkdir -p $(ZIP_SOURCES)
	cp $(NETLOGO)/$(NETLOGO_JAR) lib/
	# compile scala code using build.sbt; this also creates lib_managed folder, depositing "managed" jars there
	sbt update compile
	# copy "managed" jar files into a central location for inclusion in hubnet-web.jar
	find lib_managed -name "*.jar" -exec cp {} $(ZIP_SOURCES) \;
	# create hubnet-web.jar extension
	jar cmf manifests/web.txt $(TARGET) -C target/$(SCALA_VERSION)/classes/ . -C $(ZIP_SOURCES)/ .
	make zipfile

zipfile:
	# copy a few extra files into directory to add to zip file
	cp manifest.txt $(ZIP_SOURCES)/
	cp README.md $(ZIP_SOURCES)/
	cp $(TARGET) $(ZIP_SOURCES)/
	# create zip file
	zip -r $(ZIPTARGET) $(ZIP_SOURCES)

clean:
	# remove the extension
	rm -f $(TARGET)
	# remove the zip file
	rm -f $(ZIPTARGET).zip
	# remove the folder that creates the zip file
	rm -rf $(ZIP_SOURCES)
	# clean out all managed libraries 
	sbt clean
	# remove the "unmanaged" NetLogo.jar - it gets replaced by current version in make process
	rm -rf lib
	rm -rf project


