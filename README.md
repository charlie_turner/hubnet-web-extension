# hubnet-web-extension

## Hubnet-web Extension for NetLogo 5.0.x

This NetLogo extension provides a websocket interface to the NetLogo integrated modeling environment.
It allows websocket-based clients (e.g. browsers, iphones, etc.) to communicate with NetLogo using the
HubNet protocol. This repository was originally cloned from NetLogo's HubNet-Web-Extension written by Josh Cough.
This version comes with an expanded Javascript client that runs on iOS devices.  A single ZIP file is provided
that allows for use of the extension in compiled form.  In order to work with the extension in its precompiled
form you should be familiar with NetLogo and Hubnet (on the server side), HTML and Javascript (on the client side).
In order to work with the extension at the source level you should also be familiar Scala, sbt, and websockets.

## CONTENTS:
In addition to the source, which expects to be compiled against a 5.0.x version of NetLogo this repo contains a 
ZIP file that can be unzipped and used without compilation.

The following must be installed prior to building this extension from source.

* Scala 2.9.2
* sbt 0.12.3 or greater
* NetLogo 5.0.4

The ZIP file also contains a sample NetLogo model and basic html/javascript example that corresponds to it.  

## TO USE:

### Using the supplied ZIP file
For a first use without compiling code, do the following:

1. Acquire the NetLogo software (5.0.5 is the latest version as of this writing).
2. Place the hubnet-web.zip file from this repository into the extensions subfolder of your NetLogo installation
3. unzip, resulting in a folder called "hubnet-web" under extensions.
4. In NetLogo open the test model from this repository: **src/main/netlogo/Turtle.nlogo**; Click *Setup*, then *Go*; 
5. Write down the IP address shown in the **HubNet Control Center** window.
6. In a Browser (e.g. Safari), open the test HTML program from this repository: **src/main/html/turtle/index.html**.
7. Click the *Setup* button, then enter your initials and the IP address you recorded in step 5; click *Save*.
8. Click *Move Turtle*, then click *Connect to NetLogo*. You should see a message saying: "Login complete for <your initials>".
9. At this point, you can click arrow keys in the Turtle webpage.  The same moves should appear in the NetLogo graph.

### Using the source code
If you would like to use the source code for this extension, the following installation steps are required.

1. Acquire and install the NetLogo software (5.0.5 is the latest version as of this writing).
2. Install Scala 2.9.2.
3. Install sbt 0.12.3 or later
4. clone this project into the *NetLogo/extensions/hubnet-web* directory
5. cd hubnet-web
6. Edit the Makefile, changing the path of *NETLOGO="../../NetLogo_5.0.5"* so that it is correct for your installation. 
7. make (this should download numerous jar files for unfiltered, ultimately generating hubnet-web.jar)
8. It will also produce a hubnet-web.zip file that can be copied to the extensions folder of NetLogo for use.

## NOTES:

This version of the code upgraded to use Unfiltered 0.6.8 (websocket libraries) is the ultimate driver due to array indexing errors
when trying to send base64 images across the websocket. Bug reports on the web indicate this was a buffer overflow problem in early versions and was fixed prior to 0.6.8.

extensions [hubnet-web]

## QUESTIONS:
If you run into problems or have questions about the extension, please email me: cjturner@ucdavis.edu
I have tested early versions of this code on Mac only. 

## REFERENCES:

**sbt (Scala Build Tool)**
	http://www.scala-sbt.org/

**Unfiltered**
	http://unfiltered.databinder.net/

**websocket protocol**
	http://tools.ietf.org/html/rfc6455



